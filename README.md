# Introduction

A reducer is a function so basically if we create a store and pass any function to
it then our simplest store is ready,

**src/index.js**
```
import { createStore } from 'redux';

const reducer = () => {};

const store = createStore(reducer);
```

This way we have our very basic Redux store ready.

Now to be more specific, a reducer function accepts two arguments,

1. `state`
2. `action`

```
const reducer = (state, action) => {
    return { name: 'Pranav' };
}
```

For now we just return a object, we can access our redux state using, `store.getState()`

```
const store = createStore(reducer);

console.log(store.getState());
```

On checking browser console, you will get object `{ name: "Pranav" }`.

**Now what is the use of `action` argument ?**

Reducer catches the actions and accordingly changes and returns the state. For example,
we can dispatch an action using,

```
const action = {
    type: 'UPDATE_STATE',
    payload: {
        name: 'Alex'
    }
};

store.dispatch(action);
```

An action is simply a object and we can use `.dispatch()` from store to dispatch an
action.

But our reducer function does not handle this action yet,

```
const reducer = (state, action) => {
    if (action.type === 'UPDATE_STATE') {
        return { name: action.payload.name };
    }
    return { name: 'Pranav' };
}
```

Now if we console log the state, we get the changed state from `name: 'Pranav'` to `name: 'Alex'`.

We can simplify the action argument by breaking it,

```
const reducer = (state, { type, payload }) => {
    if (type === 'UPDATE_STATE') {
        return { name: payload.name };
    }
    return { name: 'Pranav' };
}
```

Now since our reducer has the current state as its first argument, why don't we just
return the state if none of the type matches.

**If we just return the state argument then we get an error saying**
```
Error: Reducer "person" returned undefined during initialization. If the state passed to the reducer is undefined, you must explicitly return the initial state. The initial state may not be undefined. If you don't want to set a value for this reducer, you can use null instead of undefined.
```

So we need a initialization state to be returned and not an undefined variable, we can
simply use,

```
const reducer = (state = {}, { type, payload }) => {
    if (type === 'UPDATE_STATE') {
        return { name: payload.name }
    }
    return state; // simply return state
}
```

## Object.assign
Suppose we have multiple key value pairs in the initial scheme of our redux state and
we only need to change state when a particular action type matches, in this case we
can use `Object.assign()`

Instead of just returning `return { name: payload.name }` which overrides any other
values inside our state and just gives us a new object with just `name` in it,
```
  switch(type) {
    case UPDATE_PERSON:
      return { name: payload.name };
    default:
      return state;
  }
```

**We can use,**
```
  switch(type) {
    case UPDATE_PERSON:
      return Object.assign({}, state, { name: payload.name });
    default:
      return state;
  }
```

This way we get both our initial state and we change the particular values inside our
state too.

# Testing Things
Using `jest`, we can test our our redux actions and reducers.

Install `jest` and `babel-jest` using `npm i --save jest babel-jest`

Modify `scripts` to run tests when we run a command,

```
  "scripts": {
    ...
    "test-jest": "jest",
    "test:watch": "npm run test-jest -- --watch",
  }
```

Now run `npm run test:watch`, this command will in turn run `test-jest` with `--watch`
parameter so that we can make changes and run tests alongside.

Delete the default file **src/App.test.js** and create another file in **src/actions**,

**src/action/personActions.test.js**
```
import updatePerson from '../personActions';
import { UPDATE_PERSON } from '../../Types';

describe('actions', () => {
  it('Should return action for person', () => {
    const expectedAction = {
      type: UPDATE_PERSON,
      payload: {
        name: undefined
      }
    };

    expect(updatePerson()).toEqual(expectedAction);
  })
})
```

On checking the terminal you can find that our test is running and we can see the result
of `Passed` or `Failed`.

Similarly we can write tests for **reducers**,

**src/reducers/tests/personReducer.test.js**
```
import personReducer from '../personReducer';
import { UPDATE_PERSON } from '../../Types';

describe('reducer', () => {
    const expectedState = {
        name: undefined
    }

    it('Should update person name only', () => {
        expect(
            personReducer({}, {
                type: UPDATE_PERSON,
                payload: {
                    name: undefined
                }
            })
        ).toEqual(expectedState)
    })
})
```

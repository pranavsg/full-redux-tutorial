import React, { Component } from 'react';
import { connect } from 'react-redux';

import updateGame from '../store/actions/gameActions';

class Game extends Component {
  constructor(props) {
    super(props);

    this.state = {
      game: props.game.name
    }
  }

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  handleClick = e => {
    this.props.onUpdateGame(this.state.game)
  }

  render() {
    return(
      <div>
        <h2>{this.props.game.name}</h2>
        <br />
        <input
          name="game"
          type="text"
          value={this.state.game}
          onChange={this.handleChange}
        />
        <button onClick={this.handleClick}>Change Game</button>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  game: state.game
})

const mapDispatchToProps = {
  onUpdateGame: updateGame
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Game);

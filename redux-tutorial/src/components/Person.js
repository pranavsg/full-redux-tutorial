import React, { Component } from 'react';
import { connect } from 'react-redux';

import updatePerson from '../store/actions/personActions';

class Person extends Component {
  onUpdatePerson = e => {
    this.props.onUpdatePerson(e.target.value);
  }

  render() {
    return (
      <div>
        <h1>{this.props.person.name}</h1>
        <input
          type='text'
          onChange={this.onUpdatePerson}
          value={this.props.person.name}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  person: state.person
});

const mapDispatchToProps = {
  onUpdatePerson: updatePerson
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Person);

import React, { Component } from 'react';
import { connect } from 'react-redux';

import fetchPosts from '../store/actions/postActions';

class Posts extends Component {
  onFetchPosts = e => {
    this.props.onFetchPosts();
  }

  render() {
    return (
      <div>
        <button onClick={this.onFetchPosts}>Fetch Posts</button>
        <div>
          {
            this.props.posts.length === 0 ? <h2>No Posts Found</h2> :
            this.props.posts.map(post => <p key={post.id}>{post.title}</p>)
          }
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  posts: state.posts
})

const mapDispatchToProps = {
  onFetchPosts: fetchPosts
}

export default connect(mapStateToProps, mapDispatchToProps)(Posts);

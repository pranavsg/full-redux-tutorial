export const UPDATE_PERSON = 'users:UPDATE_PERSON';

export const UPDATE_GAME = 'game:UPDATE_GAME';

export const FETCH_POSTS = 'post:FETCH_POSTS';

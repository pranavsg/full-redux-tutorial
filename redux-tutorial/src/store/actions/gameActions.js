import { UPDATE_GAME } from '../Types';

const updateGame = newGame => ({
  type: UPDATE_GAME,
  payload: {
    name: newGame
  }
});

export default updateGame;

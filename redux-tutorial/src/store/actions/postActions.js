import { FETCH_POSTS } from '../Types';

const fetchPosts = () => {
  return dispatch => {
    fetch('https://jsonplaceholder.typicode.com/posts/')
      .then(res => res.json())
      .then(json => dispatch({
        type: FETCH_POSTS,
        payload: json
      }))
  }
}

export default fetchPosts;

import { UPDATE_PERSON } from '../Types';

const updatePerson = newName => ({
  type: UPDATE_PERSON,
  payload: {
    name: newName
  }
});

export default updatePerson;

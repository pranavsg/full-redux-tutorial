import updatePerson from '../personActions';
import { UPDATE_PERSON } from '../../Types';

describe('actions', () => {
    it('Should return action for Person', () => {
        const expectedAction = {
            type: UPDATE_PERSON,
            payload: {
                name: undefined
            }
        };

        expect(updatePerson()).toEqual(expectedAction);
    })
})

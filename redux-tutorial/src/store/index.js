import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

import schema from './schema';
import allReducers from './reducers';

const middlewares = [thunk];

const store = createStore(
  allReducers,
  schema,
  compose(
    applyMiddleware(...middlewares),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
);

export default store;

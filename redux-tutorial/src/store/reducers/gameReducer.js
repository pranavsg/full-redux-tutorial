import { UPDATE_GAME } from '../Types';

const gameReducer = (state = {}, { type, payload }) => {
  switch(type) {
    case UPDATE_GAME:
      return { name: payload.name };
    default:
      return state;
  }
};

export default gameReducer;

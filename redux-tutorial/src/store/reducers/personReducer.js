import { UPDATE_PERSON } from '../Types';

const personReducer = (state = {}, { type, payload }) => {
  switch(type) {
    case UPDATE_PERSON:
      return Object.assign({}, state, { name: payload.name });
    default:
      return state;
  }
};

export default personReducer;

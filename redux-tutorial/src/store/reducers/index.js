import { combineReducers } from 'redux';

import personReducer from './personReducer';
import gameReducer from './gameReducer';
import postReducer from './postReducer';

const allReducers = combineReducers({
  person: personReducer,
  game: gameReducer,
  posts: postReducer
})

export default allReducers;

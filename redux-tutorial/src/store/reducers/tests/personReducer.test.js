import personReducer from '../personReducer';
import { UPDATE_PERSON } from '../../Types';

describe('reducer', () => {
    const expectedState = {
        name: undefined
    }

    it('Should update person name only', () => {
        expect(
            personReducer({}, {
                type: UPDATE_PERSON,
                payload: {
                    name: undefined
                }
            })
        ).toEqual(expectedState)
    })
})

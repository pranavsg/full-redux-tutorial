import { FETCH_POSTS } from '../Types';

const postReducer = (state = {}, { type, payload }) => {
  switch(type) {
    case FETCH_POSTS:
      return payload;
    default:
      return state;
  }
};

export default postReducer;

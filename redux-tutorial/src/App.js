import React from 'react';
import './App.css';

import Person from './components/Person';
import Game from './components/Game';
import Posts from './components/Posts';

function App() {
  return (
    <div className="App">
      <h1>Redux React Tutorial</h1>
      <Person />
      <Game />
      <Posts />
    </div>
  );
}

export default App;
